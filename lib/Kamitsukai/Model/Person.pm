use strict;
use warnings;

package Kamitsukai::Model::Person;
use Jifty::DBI::Schema;

use Kamitsukai::Record schema {
    column name =>
        type is 'text',
        label is 'Name';
};

# Your model-specific methods go here.

1;

