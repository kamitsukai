use strict;
use warnings;

package Kamitsukai::Model::User;
use Jifty::DBI::Schema;

use Kamitsukai::Record schema {
    column userpic      => is Userpic;
    column geo_location => is GeoLocation;
};

use Jifty::Plugin::Authentication::Bitcard::Mixin::Model::User;

# Your model-specific methods go here.

sub _value
{
    my $self = shift;
    my $column = shift;

    return $self->__value($column);
}

1;

