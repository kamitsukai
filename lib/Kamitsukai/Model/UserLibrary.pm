use strict;
use warnings;

package Kamitsukai::Model::UserLibrary;
use Jifty::DBI::Schema;

use Kamitsukai::Record schema {
    column user_id =>
        references Kamitsukai::Model::User
        is immutable,
        is indexed;
    column item_id =>
        type is 'int(11)',
        max_length is 11,
        is immutable,
        is indexed;
    column item_type =>
        type is 'varchar(10)',
        max_length is 10,
        is immutable,
        is indexed;
    column owned =>
        type is 'tinyint(1)',
        max_length is 1;
    column num_owned =>
        type is 'int(11)',
        max_length is 11;
    column want =>
        type is 'tinyint(1)',
        max_length is 1;
    column offered =>
        type is 'tinyint(1)',
        max_length is 1;
    column date_created =>
        type is 'datetime';
    column date_updated =>
        type is 'datetime';
};

# Your model-specific methods go here.

1;

