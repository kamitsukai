use strict;
use warnings;

package Kamitsukai::Model::ItemPersonXrf;
use Jifty::DBI::Schema;

use Kamitsukai::Record schema {
    column item =>
        type is 'int(11)',
        max_length is 11,
        is immutable;
    column item_type =>
        type is 'varchar(10)',
        max_length is 10,
        is immutable;
    column person =>
        references Kamitsukai::Model::Person,
        is immutable;
    column role =>
        type is 'varchar(255)',
        max_length is 255,
        is immutable;
};

# Your model-specific methods go here.

1;

