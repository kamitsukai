use strict;
use warnings;

package Kamitsukai::Model::Book;
use Jifty::DBI::Schema;

use Kamitsukai::Record schema {
    column isbn =>
        type is 'varchar(13)',
        label is 'ISBN',
        max_length is 13,
        is indexed;
    column ean =>
        type is 'varchar(13)',
        label is 'EAN',
        max_length is 13,
        is indexed;
    column asin =>
        type is 'varchar(255)',
        label is 'ASIN',
        max_length is 255,
        is indexed;
    column title =>
        type is 'text',
        label is 'Title';
    column binding =>
        type is 'varchar(255)',
        label is 'Binding',
        max_length is 255;
    column dewey_decimal =>
        type is 'float',
        label is 'Dewey Decimal';
    column publisher =>
        type is 'text',
        label is 'Publisher';
    column published =>
        type is 'date',
        label is 'Publication Date';
    column released =>
        type is 'date',
        label is 'Released On';
    column studio =>
        type is 'text',
        label is 'Studio';
    column pages =>
        type is 'int(11)',
        label is 'Number of Pages',
        max_length is 11;
    column height =>
        type is 'int(11)',
        label is 'Height',
        max_length is 11;
    column height_units =>
        type is 'varchar(255)',
        label is 'Height Units',
        max_length is 255;
    column 'length' =>
        type is 'int(11)',
        label is 'Length',
        max_length is 11;
    column length_units =>
        type is 'varchar(255)',
        label is 'Length Units',
        max_length is 255;
    column width =>
        type is 'int(11)',
        label is 'Width',
        max_length is 11;
    column width_units =>
        type is 'varchar(255)',
        label is 'Width Units',
        max_length is 255;
    column weight =>
        type is 'int(11)',
        label is 'Weight',
        max_length is 11;
    column weight_units =>
        type is 'varchar(255)',
        label is 'Weight Units',
        max_length is 255;
    column detail_page_url =>
        type is 'text',
        label is 'Amazon Detail Page';
    column date_created =>
        type is 'datetime',
        label is 'Created On';
    column date_updated =>
        type is 'datetime',
        label is 'Updated On',
        is indexed;
};

# Your model-specific methods go here.

1;

