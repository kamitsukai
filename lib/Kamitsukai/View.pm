package Kamitsukai::View;

use strict;
use warnings;
use vars qw( $r );

use Jifty::View::Declare -base;

use Scalar::Defer;

=head1 NAME

Jifty::Plugin::SkeletonApp::View

=head1 DESCRIPTION

This somewhat-finished (But not quite) template library implements
Jifty's "pony" Application. It could certainly use some
refactoring. (And some of the menu stuff should get factored out into
a dispatcher or the other plugins that implement it.


=cut

private template 'salutation' => sub {
    div {
    attr {id => "salutation" };
        if (    Jifty->web->current_user->id
            and Jifty->web->current_user->user_object )
        {
            _( 'Hiya, %1.', Jifty->web->current_user->username );
        }
        else {
#            _("You're not currently signed in.");
        }
    }
};

private template 'menu' => sub {
    div {
    attr { id => "navigation" };
        Jifty->web->navigation->render_as_menu; };
};

template '__jifty/empty' => sub :Static {
        '';
};


private template 'header' => sub {
    my ($title) = get_current_attr(qw(title));
    Jifty->handler->apache->content_type('text/html; charset=utf-8');
    head { 
        with(
            'http-equiv' => "content-type",
            content      => "text/html; charset=utf-8"
          ),
          meta {};
        with( name => 'robots', content => 'all' ), meta {};
        title { _($title) };
        Jifty->web->include_css;
        Jifty->web->include_javascript;
    };
};

private template 'heading_in_wrapper' => sub {
    h1 { attr { class => 'title' }; outs_raw(get('title')) };
};

private template 'headers' => sub {
    my $self = shift;
    my $title = shift || Jifty->config->framework('ApplicationName');

    div {
        attr { id => "headers" };
    };
};

private template 'keybindings' => sub {
    div { id is "keybindings";
      outs_raw('<script type="text/javascript">Jifty.KeyBindings.reset()</script>') };
};

#template 'index.html' => page { { title is _('Welcome to your new Jifty application') } img { src is "/static/images/pony.jpg", alt is _( 'You said you wanted a pony. (Source %1)', 'http://hdl.loc.gov/loc.pnp/cph.3c13461'); }; };

template login_widget => sub {

    my ( $action, $next ) = get( 'action', 'next' );
    $action ||= new_action( class => 'Login' );
    $next ||= Jifty::Continuation->new(
        request => Jifty::Request->new( path => "/" ) );
    show('headers', 'Login');
    span {
        attr { id => 'page-title' };
        outs(_( "Login" ));
    };
    unless ( Jifty->web->current_user->id ) {
        div {
            attr { id => 'jifty-login' };
            Jifty->web->form->start( call => $next );
            render_param( $action, 'email', focus => 1 );
            render_param( $action, $_ ) for (qw(password remember));
            form_return( label => _(q{Login}), submit => $action );
            hyperlink(
                label => _("Lost your password?"),
                url   => "/lost_password"
            );
            Jifty->web->form->end();
        };
        p {
            outs( _( "No account yet? It's quick and easy. " ));
            tangent( label => _("Sign up for an account!"), url   => '/signup');
        };
    } else {
        outs( _("You're already logged in.") );
    }
};

1;
