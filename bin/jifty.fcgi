#!/bin/sh

# Where should we look for Perl modules installed outside
# of the system directories
#PERL_DIR="/home/jhelwig/perlmods"

#export PERL5LIB=`/home/jhelwig/projects/kamitsukai/bin/setup_perl5lib.pl $PERL_DIR`

# Make sure /usr/sbin is in the path so we can find sendmail
export PATH=$PATH:/usr/sbin

# Let Jifty know we're running as a FastCGI script.
export JIFTY_COMMAND=fastcgi

# This should be the full path to the deployed bin/jifty
# in the package directory.
/home/jhelwig/projects/kamitsukai/bin/jifty
