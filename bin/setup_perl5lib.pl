#!/usr/bin/perl -w

use strict;

my $perldir = $ARGV[0];
my @additional_vers = @ARGV[1 .. $#ARGV];

my $perlver = $];
$perlver =~ s/^(\d+)\.(\d\d\d)(\d\d\d)$/"$1.".(0+$2).".".(0+$3)/e;
my ($majorver) = ($perlver =~ m/^(\d+\.\d+)/);
my @baselibs = qw( lib/perl share/perl );
my @libs;
for my $ver ($perlver, $majorver, @additional_vers) {
    for (@baselibs) {
        push @libs, "$perldir/$_/$ver";
    }
}
print join(":", @libs);
